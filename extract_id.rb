def execute(input)
  return input if input.is_a?(Integer)

  if input.is_a?(Hash)
    extract_hash_ids(input)
  elsif input.is_a?(Array)
    extract_array_values(input)
  end
end

def extract_hash_ids(hash)
  hash.values.map { |value| execute(value) }.flatten
end

def extract_array_values(array)
  array.map { |value| value.is_a?(Hash) ? extract_hash_ids(value) : value }
end