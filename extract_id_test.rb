require_relative 'extract_id'
require 'test/unit'

class ExtractIdTest < Test::Unit::TestCase
  def test_case_1
    input = {foo: [1], bar: { some: 2, property: 3 }, some: 4 }
    assert_equal [1, 2, 3, 4], execute(input)
  end

  def test_case_2
    input_2 = {foo: [1], bar: { some: [2], property: 3, some_other: { nested: 4, dog: [5] }}}
    assert_equal [1, 2, 3, 4, 5], execute(input_2)
  end

  def test_case_3
    input_3 = {id: 1, items: [ {foo: 2}, {bar: 3, dog: [{cat: 4}, {human: 5}]}]}
    assert_equal [1, 2, 3, 4, 5], execute(input_3)
  end
end
